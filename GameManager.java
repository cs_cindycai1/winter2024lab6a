public class GameManager {
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	//!![this.] is not mandatory when there is no similar variable name but add specification
	//CONSTRUCTOR
	public GameManager() {
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	//GET METHODS
	public Deck getDrawPile() {
		return this.drawPile;
	}
	
	public Card getCenterCard() {
		return this.centerCard;
	}
	
	public Card getPlayerCard() {
		return this.playerCard;
	}
	
	//TOSTRING
	public String toString() {
		return "Center Card: " + this.centerCard + "\nPlayer Card: " + this.playerCard;
	}
	
	//CUSTOM METHODS
	//shuffle drawPile, update player and center card to new cards
	public void dealCards() {
		this.drawPile.shuffle();
		this.playerCard = this.drawPile.drawTopCard();
		this.centerCard = this.drawPile.drawTopCard();
	}
	
	//return remaining number of cards in drawPile
	public int getNumberOfCards() {
		return this.drawPile.getNumberOfCards();
	}
	
	//compare value and suit of playerCard and centerCard and return points associated with match
	public int calculatePoint() {
		if (this.playerCard.getValue().equals(this.centerCard.getValue())) {
			return 4;
		} else if (this.playerCard.getSuit().equals(this.centerCard.getSuit())) {
			return 2;
		}
		
		return -1;
	}
}