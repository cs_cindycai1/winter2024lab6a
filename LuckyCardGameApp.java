import java.util.Scanner;

public class LuckyCardGameApp {
	public static void main(String[] args) {
		GameManager manager = new GameManager();
		int totalPoints = 0;
		
		System.out.println("Welcome to the Lucky! Card Game!");
		
		int round = 0;
		
		//continue game until player has 5 points or there is no card left
		while (manager.getDrawPile().getNumberOfCards() > 1 && totalPoints < 5) {
			round++;
			
			System.out.println("ROUND: " + round);
			System.out.println("--------------------------");
			System.out.println(manager);
			System.out.println("--------------------------");
			
			totalPoints += manager.calculatePoint();
			
			System.out.println("Point: " + totalPoints);
			System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			
			manager.dealCards();
		}
		
		//print result of game
		System.out.println("GAME END!");
		if (totalPoints < 5) {
			System.out.print("Player loses with: ");
		} else {
			System.out.print("Player wins with: ");
		} 
		System.out.println(totalPoints);
	}
}