import java.util.Random;

public class Deck {
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	//CONSTRUCTOR
	public Deck() {
		rng = new Random();
		numberOfCards = 52;
		
		this.cards = new Card[numberOfCards];
		
		String[] suits = new String[] {"Hearts", "Spades", "Diamonds", "Clubs"};
		String[] value = new String[] {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
		
		int valueIndex = 0;
		int suitIndex = 0;
		for (int i = 0; i < this.cards.length; i++) {
			if (valueIndex == value.length) {
				valueIndex = 0;
				suitIndex++;;
			}
			this.cards[i] = new Card(suits[suitIndex], value[valueIndex]);
			valueIndex++;
		}
		
		/* another way*
		int cardCounter = 0;
		while (cardCounter < this.numberOfCards) {
			for (int i = 0; i < suits.length; i++) {
				for (int j = 0; j < value.length; j++) {
					this.cards[cardCounter] = new Card(suits[i], value[j]);
					cardCounter++;
				}
			}
		} */
		
	}
	
	//GET METHODS
	public int getNumberOfCards() {
		return this.numberOfCards;
	}
	
	//SET METHODS
	public void setNumberOfCards(int num) {
		this.numberOfCards -= num;
	}
	
	//TOSTRING
	public String toString() {
		String result = "";
		
		for (int i = 0; i < this.numberOfCards; i++) {
			result += "\nCard " + (i + 1) + ": " + this.cards[i];
		}
		
		return result;
	}
	
	//CUSTOM METHODS
	//return last card of the deck and subtract the number of card by 1
	public Card drawTopCard() {
		numberOfCards--;
		return this.cards[numberOfCards];
	}
	
	//shuffle the deck
	public void shuffle() {
		for (int i = 0; i < this.numberOfCards; i++) {
			int switchIndex = rng.nextInt(numberOfCards);
			Card temp = this.cards[i];
			this.cards[i] = this.cards[switchIndex];
			this.cards[switchIndex] = temp;
		}
	}
}